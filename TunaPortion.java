package a4;

public class TunaPortion implements IngredientPortion{
	private static Ingredient Tuna = new Tuna(); 
	private double amount = 0;
	
	public TunaPortion (double amount) {
		if (amount < 0) {
			throw new RuntimeException("Amount is less than 0"); }
	
	this.amount = amount; 
	}

	public Ingredient getIngredient() {
		return Tuna;
	}

	public String getName() {
		return Tuna.getName();
	}

	public double getAmount() {
		return amount;
	}

	public double getCalories() {
		return Tuna.getCaloriesPerOunce() * amount;
	}

	public double getCost() {
		return Tuna.getPricePerOunce() * amount;
	}

	public boolean getIsVegetarian() {
		return Tuna.getIsVegetarian();
	}

	public boolean getIsRice() {
		return Tuna.getIsRice();
	}

	public boolean getIsShellfish() {
		return Tuna.getIsShellfish();
	}

	public IngredientPortion combine(IngredientPortion other) {
		if (other == null) {
			return this; 
		} else if (!this.getName().equals(other.getName())) {
			throw new RuntimeException("Not the same ingregient"); 
		} else {
			TunaPortion ShrimpTuna = new TunaPortion(this.getAmount() + other.getAmount());
			return ShrimpTuna;
		}
	}	
}
