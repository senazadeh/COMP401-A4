package a4;

public class Nigiri implements Sushi{
	private NigiriType type = null;
	private IngredientPortion seafood = null;
	private double nigriAmount = 0.75;
	private IngredientPortion rice = new RicePortion(0.5);
	
	public enum NigiriType {TUNA, YELLOWTAIL, EEL, CRAB, SHRIMP};
	
	public Nigiri(NigiriType type) {
		this.type = type;	
	
	if (this.type == NigiriType.TUNA) {
		this.seafood = new TunaPortion(nigriAmount);
	}
	if (this.type == NigiriType.YELLOWTAIL) {
		this.seafood = new YellowtailPortion(nigriAmount);
	}
	if (this.type == NigiriType.EEL) {
		this.seafood = new EelPortion(nigriAmount);
	}
	if (this.type == NigiriType.CRAB) {
		this.seafood = new CrabPortion(nigriAmount);
	}
	if (this.type == NigiriType.SHRIMP) {
		this.seafood = new ShrimpPortion(nigriAmount);
	}
}	
	
	public String getName() {
		return this.seafood.getName() + " nigiri";
	}

	public IngredientPortion[] getIngredients() {
		IngredientPortion[] nigiriPortions = new IngredientPortion[] {this.seafood, this.rice};		
		return nigiriPortions;
	}

	public int getCalories() {
		return (int) (this.seafood.getCalories() + this.rice.getCalories() + 0.5);
	}

	public double getCost() {
		return this.seafood.getCost() + this.rice.getCost();
	}

	public boolean getHasRice() {
		return false;
	}

	public boolean getHasShellfish() {
		return this.seafood.getIsShellfish();
	}

	public boolean getIsVegetarian() {
		return false;
	}

}

