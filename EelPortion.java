package a4;

public class EelPortion implements IngredientPortion{
	private static Ingredient Eel = new Eel(); 
	private double amount = 0;
	
	public EelPortion (double amount) {
		if (amount < 0) {
			throw new RuntimeException("Amount is less than 0"); }
	
	this.amount = aamount; 
	}

	public Ingredient getIngredient() {
		return Eel;
	}

	public String getName() {
		return Eel.getName();
	}

	public double getAmount() {
		return amount;
	}

	public double getCalories() {
		return Eel.getCaloriesPerOunce() * amount;
	}

	public double getCost() {
		return Eel.getPricePerOunce() * amount;
	}

	public boolean getIsVegetarian() {
		return Eel.getIsVegetarian();
	}

	public boolean getIsRice() {
		return Eel.getIsRice();
	}

	public boolean getIsShellfish() {
		return Eel.getIsShellfish();
	}

	public IngredientPortion combine(IngredientPortion other) {
		if (other == null) {
			return this; 
		} else if (!this.getName().equals(other.getName())) {
			throw new RuntimeException("Not the same ingregient"); 
		} else {
			EelPortion combinedEel = new EelPortion(this.getAmount() + other.getAmount());
			return combinedEel;
		}
	}	
}
